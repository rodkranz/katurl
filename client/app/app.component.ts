// Angular
import {Component}                          from 'angular2/core';
import {
            RouteConfig, ROUTER_PROVIDERS,
            RouterOutlet, Route
       }                                    from 'angular2/router'

// Mine
import {NavbarComponent}                    from './navbar/navbar.component';
import {FooterComponent}                    from './footer/footer.component';
import {DashboardComponent}                 from './dashboard/dashboard.component';
import {AboutComponent}                     from './about/about.component';
import {DetailComponent}                    from './detail/detail.component';
import {ContactComponent}                   from './contact/contact.component';

// Main Component
@Component({
      selector      : 'k-app'
    , templateUrl   : './app/app.component.html'
    , styleUrls     : ['./app/app.component.css']
    , directives    : [NavbarComponent, FooterComponent, RouterOutlet]
    , providers     : [ROUTER_PROVIDERS]
})

// Routers
@RouteConfig([
      new Route({path: '/Dashboard',    name: 'Dashboard',  component: DashboardComponent,   useAsDefault: true })
    , new Route({path: '/About',        name: 'About',      component: AboutComponent   })
    , new Route({path: '/Detail',       name: 'Detail',     component: DetailComponent  })
    , new Route({path: '/Contact',      name: 'Contact',    component: ContactComponent })
])

// Export
export class AppComponent {}
