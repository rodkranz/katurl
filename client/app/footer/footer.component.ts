import {Component} from 'angular2/core';

@Component({
      selector    : 'k-footer'
    , templateUrl : './app/footer/footer.component.html'
    , styleUrls   : ['./app/footer/footer.component.css']
})

export class FooterComponent {
    text: string;

    constructor() {
        this.text = 'Copyright &copy; 2016';
    }

    getTextFooter() {
        return this.text;
    }
}