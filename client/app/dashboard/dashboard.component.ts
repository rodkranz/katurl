import {Component}  from 'angular2/core';

@Component({
      selector      : 'k-home'
    , templateUrl   : '/app/dashboard/dashboard.component.html'
    , styleUrls     : ['/app/dashboard/dashboard.component.css']
})

export class DashboardComponent {}