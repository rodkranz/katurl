/**
 * This file launch the application
 */

import {bootstrap}          from 'angular2/platform/browser'
import {ROUTER_PROVIDERS}   from 'angular2/router';
import {AppComponent}       from './app.component'

//enableProdMode();
bootstrap(AppComponent, [ROUTER_PROVIDERS]);