import {Component, Output, EventEmitter} from "angular2/core";

@Component({
      selector: 'loader'
    , template: `
        <div [hidden]="visible">
            Loading....
        </div>
        <div [hidden]="!visible">
            <ng-content></ng-content>
        </div>
    `
})

export class LoaderComponent {
    private visible: boolean = false;

    constructor(broadcaster: Broadcaster) {
        setInterval(() => {
            broadcaster.next(this.generatedNumber = Math.random());
        },1000);
    }


    public show(): void {
        this.visible = true;
    }

    public hide(): void {
        this.visible = false;
    }
}
