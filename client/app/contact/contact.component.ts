import {Component, Input}   from 'angular2/core';
import {
            FormBuilder, Validators,
            ControlGroup, Control,
            FORM_DIRECTIVES
        }                   from 'angular2/common';
import {Message}            from './message';
import {emailValidator}     from './../common/validators';
import {LoaderComponent}    from './../common/loading';

@Component({
      selector      : 'k-contact'
    , templateUrl   : './app/contact/contact.component.html'
    , styleUrls     : ['./app/contact/contact.component.css']
    , directives    : [FORM_DIRECTIVES, LoaderComponent]
    , providers     : [FormBuilder]
    , viewProviders : []
    , inputs        : ['message']
})

export class ContactComponent {
    @Input()
    public    form    : ControlGroup;
    public    message : Message;

    constructor(fb: FormBuilder) {
        this.form   = fb.group({
            'name'  : new Control('', Validators.required),
            'email' : new Control('', Validators.required),
            'text'  : new Control('', Validators.required)
        });
    }

    onSubmit(): void {
        let payLoad = JSON.stringify(this.form.value);
            console.log(payLoad);
    }
}