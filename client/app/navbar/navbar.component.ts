import {Component}                           from 'angular2/core';
import {ROUTER_DIRECTIVES, Location, Router} from 'angular2/router';

@Component({
      selector:     'k-navbar'
    , templateUrl:  './app/navbar/navbar.component.html'
    , styleUrls:    ['./app/navbar/navbar.component.css']
    , directives:   [ROUTER_DIRECTIVES]
})

export class NavbarComponent {
    router  : Router;
    location: Location;

    constructor(router: Router, location: Location) {
        this.router     = router;
        this.location   = location;
    }

    getLinkStyle(path: string) {
        return this.location.path() === path;
    }

}
