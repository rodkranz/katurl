import {Component} from 'angular2/core';

@Component({
      selector      : 'k-detail'
    , templateUrl   : './app/detail/detail.component.html'
    , styleUrls     : ['./app/detail/detail.component.css']
})

export class DetailComponent {}