import {Component}              from 'angular2/core';
import {RouteConfig, Route}     from 'angular2/router';

@Component({
      selector      : 'k-about'
    , templateUrl   : './app/about/about.component.html'
    , styleUrls     : ['./app/about/about.component.css']
})

export class AboutComponent {}