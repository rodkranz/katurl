package main

import (
	"fmt"
	"log"
	"net/http"
	"strings"

	"bitbucket.org/rkranz/katurl/server/url"
	"flag"
	"encoding/json"

	"bitbucket.org/rkranz/katurl/server/minify"
)

var	(
	logOn   *bool
	port 	*int
	ssl		*bool
	urlBase string
)

func init() {
	domain := flag.String("d", "localhost", "domain")
	port	= flag.Int("p", 8888, "port")
	logOn   = flag.Bool("l", true, "Log true/false")
	ssl 	= flag.Bool("s", false, "ssl")

	flag.Parse()

	withSSL := "http";
	if *ssl {
		withSSL = "https";
	}

	urlBase = fmt.Sprintf("%s://%s:%d", withSSL, *domain, *port)
}

type Headers map[string]string

type Redirect struct {
	stats chan string
}

func (r *Redirect) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	findUrlAndExecute(w, req, func (url *url.Url) {
		http.Redirect(w, req, url.Destination, http.StatusMovedPermanently)
		r.stats <- url.Id
	})
}

func Shortener(w http.ResponseWriter, req *http.Request) {
	if req.Method != "POST" {
		responseWith(w, http.StatusMethodNotAllowed, Headers{
			"Allow": "POST",
		})
		return
	}

	url, isNew, err := url.FindOrCreateUrl(extractUrl(req))
	if err != nil {
		responseWith(w, http.StatusBadRequest, nil)
		return
	}

	status := http.StatusOK
	if isNew {
		status = http.StatusCreated
	}

	shortUrl := fmt.Sprintf("%s/r/%s", urlBase, url.Id)
	responseWith(w, status, Headers{
		"Location": shortUrl,
		"Link": 	fmt.Sprintf("<%s/api/stats/%s>", urlBase, url.Id),
	})

	logger("the URL [%s] has been shortened successfully to [%s] ", url.Destination, shortUrl)
}

func Visualizer(w http.ResponseWriter, req *http.Request) {
	findUrlAndExecute(w, req, func (url *url.Url) {
		json, err := json.Marshal(url.Stats())

		if err != nil {
			w.WriteHeader(http.StatusInternalServerError)
			return
		}

		responseWithJSON(w, string(json))
	})
}

func findUrlAndExecute(w http.ResponseWriter, r *http.Request, executor func(*url.Url)) {
	paths := strings.Split(r.URL.Path, "/")
	id 	  := paths[len(paths)-1]

	if url := url.Find(id); url != nil {
		executor(url)
	} else {
		http.NotFound(w, r)
	}
}

func responseWith(w http.ResponseWriter, status int, headers Headers) {
	for k, v := range headers {
		w.Header().Set(k, v)
	}
	w.WriteHeader(status)
}

func responseWithJSON(w http.ResponseWriter, answer string) {
	responseWith(w, http.StatusOK, Headers{"Content-Type": "application/json"})
	fmt.Fprintf(w, answer)
}

func extractUrl(r *http.Request) string {
	url := make([]byte, r.ContentLength, r.ContentLength)
	r.Body.Read(url)
	return string(url)
}

func registerStats(stats <-chan string) {
	for id := range stats {
		url.RecordClick(id)
		logger("Click has been registered successfully for %s", id)
	}
}

func logger(format string, values ...interface{}) {
	if *logOn {
		log.Printf(fmt.Sprintf("%s\n", format), values...)
	}
}


func CompactCSS(w http.ResponseWriter, req *http.Request) {
	files := []string{ "public/vendor/angular-material/angular-material.min.css",
		"public/css/main.css" }

	m := minify.NewMinifier("text/css; charset=utf-8", files)
	m.Write(w)
}

func CompactJS(w http.ResponseWriter, req *http.Request) {
	files := []string{ // vendor
		"public/vendor/angular/angular.min.js", "public/vendor/angular-animate/angular-animate.min.js",
		"public/vendor/angular-aria/angular-aria.min.js", "public/vendor/angular-messages/angular-messages.min.js",
		"public/vendor/angular-material/angular-material.min.js",
		"public/vendor/angular-ui-router/release/angular-ui-router.min.js",
		// APP
		"public/app/app.js", "public/app/ctrls/AboutCtrl.js",
 		"public/app/ctrls/ContactCtrl.js", "public/app/ctrls/HomeCtrl.js" }

	m := minify.NewMinifier("application/javascript", files)
	m.Write(w)
}

func main() {
	url.ConfigurationRepository(url.NewMemoryRepository())


	stats := make(chan string)
	defer close(stats)
	go registerStats(stats)

	// Static folder
	fs := http.FileServer(http.Dir("public"))
	http.Handle("/", fs)

	// Methods folder
	http.Handle("/r/", &Redirect{stats})

	http.HandleFunc("/api/shortener", 			Shortener)
	http.HandleFunc("/api/stats/", 	  			Visualizer)

//	http.HandleFunc("/minify/compiled.js",  	CompactJS)
//	http.HandleFunc("/minify/compiled.css", 	CompactCSS)

	logger("Server started port %d", *port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", *port), nil))
}