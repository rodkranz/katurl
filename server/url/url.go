package url

import (
	"time"
	"math/rand"
	"net/url"
)

const (
	size   = 5
	symbol = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890_-+"
)

type Repository interface {
	IdExist(id string) bool
	FindById(id string) *Url
	FindByUrl(url string) *Url
	Save(url Url) error
	RecordClick(id string)
	FindClicks(id string) int
}

type Url struct {
	Id 			string 		`bson:"_id"`
	CreatedAt 	time.Time 	`bson:"created_at"`
	Destination string		`bson:"destination"`
}

type Clicks struct {
	Id 			string 		`bson:"_id"`
	Total		int			`bson:"total"`
}

type stats struct {
	Url 	*Url `json:"url"`
	Clicks	int  `json:"clicks"`
}

var repo Repository

func init() {
	rand.Seed(time.Now().UnixNano())
}

func ConfigurationRepository(r Repository) {
	repo = r
}

func RecordClick(id string)  {
	repo.RecordClick(id)
}

func FindOrCreateUrl(destination string) (u *Url, isNew bool, err error) {
	if u = repo.FindByUrl(destination); u != nil {
		return u, false, nil
	}

	if _, err = url.ParseRequestURI(destination); err != nil {
		return nil, false, err
	}

	url := Url{generateId(), time.Now(), destination}
	repo.Save(url)
	return &url, true, nil
}

func Find(id string) *Url {
	return repo.FindById(id)
}

func (u *Url) Stats() *stats {
	clicks := repo.FindClicks(u.Id)
	return &stats{u, clicks}
}

func generateId() string {
	newId := func() string {
		id := make([]byte, size, size)
		for i := range id {
			id[i] = symbol[rand.Intn(len(symbol))]
		}
		return string(id)
	}
	for {
		if id := newId(); !repo.IdExist(id) {
			return id
		}
	}
}