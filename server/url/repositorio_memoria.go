package url

import (
	"gopkg.in/mgo.v2"
	"gopkg.in/mgo.v2/bson"
	"log"
	"os"
)

/**
 * Repository structure
 */
type memoryRepository struct {
	mdb    *mgo.Database
	urls   map[string]*Url
	clicks map[string]int
}

/**
 * Create New repository for url.
 */
func NewMemoryRepository() *memoryRepository {
	mongoSession, err := mgo.Dial("localhost")
	if err != nil {
		log.Fatal("MongoDB is not available to connect!", err)
		os.Exit(1)
	}
	mdb := mongoSession.DB("katurl")

	return &memoryRepository{
		mdb,
		make(map[string]*Url),
		make(map[string]int),
	}
}

/**
 * Verify if id exist in our repository
 */
func (r *memoryRepository) IdExist(id string) bool {
	result := Url{}
	r.mdb.C("urls").Find(bson.M{"_id": id}).One(&result)
	return len(result.Id) != 0
}

/**
 * Find url by id
 */
func (r *memoryRepository) FindById(id string) *Url {
	result := Url{}
	r.mdb.C("urls").FindId(id).One(&result)
	if len(result.Id) == 0 {
		return nil
	}
	return &result
}

/**
 * Find url by url.
 */
func (r *memoryRepository) FindByUrl(url string) *Url {
	result := Url{}
	r.mdb.C("urls").Find(bson.M{"destination": url}).One(&result)
	if len(result.Id) != 0 {
		return &result;
	}
	return nil
}

/**
 * Save url in repository
 */
func (r *memoryRepository) Save(url Url) error {
	r.mdb.C("urls").Insert(&url)
	r.mdb.C("clicks").Insert(Clicks{url.Id, 0})
	return nil
}

/**
 * Increment click at url id.
 */
func (r* memoryRepository) RecordClick(id string) {
	r.mdb.C("clicks").Update(bson.M{"_id": id}, bson.M{ "$inc": bson.M{"total": 1}});
}

/**
 * Get Total of clicks of Url id.
 */
func (r* memoryRepository) FindClicks(id string) int {
	clicks := Clicks{}
	r.mdb.C("clicks").Find(bson.M{"_id": id}).One(&clicks)
	return clicks.Total
}