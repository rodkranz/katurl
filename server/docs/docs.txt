Url Shortener
------------------------------------------------------------------------------------------------------------------------

Aplicação será bem simples é uma encurtadora de urls.
O Objectivo eh receber uma url e encurtar o link criando um segundo link para o link recebido.
Terá que guardar um Objecto com {id, createdAt, Destination, clicks}.

------------------------------------------------------------------------------------------------------------------------
1) Link Redirecionamento:
    URL:
        http://localhost:8888/r/{hash}

    Method:
        GET

    Descrição:
        Devera retornar o "status code" 301 (Moved Permanently)
        Com o "Location" da url original.
        caso ache a url.

    Error:
        Caso não encontre o objeto deve
        retornar com "status code" 404 (Not Found) uma mensagem "404 page not found"

    Exp. Curl:
        $ curl -v http://localhost:8888/r/aaaaa

    Exp. resposta (404):
        Header:
            Content-Length          → 19
            Content-Type            → text/plain; charset=utf-8
            Date                    → Tue, 29 Dec 2015 14:10:38 GMT
            X-Content-Type-Options  → nosniff

        Body:
            404 page not found

    Exp. respoista (301)
        Header:
            Location          → http://localhost:8888/LoremIpsum
            Date              → Tue, 29 Dec 2015 14:12:21 GMT
            Content-Length    → 85
            Content-Type      → text/html; charset=utf-8

        body:
            <a href="http://localhost:8888/LoremIpsum">Moved Permanently</a>.

------------------------------------------------------------------------------------------------------------------------
2) Link Criação da url
    URL:
        http://localhost:8888/api/shortener

    Method:
        POST

    Descrição:
        Devera receber no corpo do post a url que sera encurtada.
        Caso a url seja uma url nova, devera retornar com "status code" 201,
        se a url ja estiver cadastrada deverá retornar o "status code" 200,
        no header da resposta deve retornar um campo com o nome "Link" e a url para ver a
        estatisticas de quantos clickes e quando a url foi criada.
        exemplo: "Link: <http://localhost:8888/api/stats/AAAA>"
        e um campo "Location" com a url para o redirecionamento,
        exemplo: "Location: http://localhost:8888/r/AAAA"

    Exp. Curl
        $ curl -v http://localhost:8888/api/shortener -d "http://localhost:8888/LoremIpsum"

    Exp. Resposta: (200/201)
        Content-Length  → 0
        Content-Type    → text/plain; charset=utf-8
        Date            → Tue, 29 Dec 2015 14:05:11 GMT
        Link            → <http://localhost:8888/api/stats/AAAA>
        Location        → http://localhost:8888/r/AAAA


------------------------------------------------------------------------------------------------------------------------
3) Link de estatisticas
    URL:
        http://localhost:8888/api/stats/{id}

    Method:
        GET

    Descrição:
        Essa url devera retornar um json com a descrição sobre
        o objecto que estiver referenciado ao id.
        caso não ache retorne um simples 404.

    Exp. Curl:
        $ curl -v http://localhost:8888/api/stats/AAAA

    Exp. Resposta: (200)
        header:
            Content-Length  → 145
            Content-Type    → application/json
            Date            → Tue, 29 Dec 2015 14:19:03 GMT

        body:
            {
                "url": {
                    "Id": "AAAA",
                    "CreatedAt": "2015-12-29T14:02:34.522049131Z",
                    "Destination": "http://localhost:8888/LoremIpsum"
                },
                "clicks": 2
            }

    Exp. resposta (404):
        Header:
            Content-Length          → 19
            Content-Type            → text/plain; charset=utf-8
            Date                    → Tue, 29 Dec 2015 14:10:38 GMT
            X-Content-Type-Options  → nosniff

        Body:
            404 page not found

------------------------------------------------------------------------------------------------------------------------
OBS.:
    Não teremos que usar nenhum tipo de DB, usamos a memoria virtual da maquina para isso.
    eh preciso que rode a aplicação e ela se mantenha viva ate o utilizador forcar o fechamento.
    deve mostrar os logs no console ou alguma pasta de log.
    mostrar apenas 2 logs (
        2015/12/29 13:55:10 Click has been registered successfully for {ID}
        2015/12/29 14:02:18 the URL [{URL_ORIGINAL}] has been shortened successfully to [URL_SHORT]
    )
    a aplicação deve ter suporte para receber 4 parametros de configuração.

    Parametros de configuração:

        domain  : dominio onde vai rodar        default: localhost
        port    : porta na qual ira listar      default: 8888
        logOn   : mostrar logs no console       default: true
        ssl     : aceita pelo recurso https     default: false (opcional)
------------------------------------------------------------------------------------------------------------------------