package minify

import (
	"io/ioutil"
	"bytes"
	"log"
	"path/filepath"
	"net/http"
	"strconv"
)

type Minifier	struct {
	mediaType 	string
	files 		[]string
}

func NewMinifier(mediaType string, files []string) *Minifier {
	return &Minifier{mediaType, files}
}

func (m *Minifier) Write (w http.ResponseWriter)  {
	var buffCompiled bytes.Buffer

	for _, file := range m.files {
		readFile(file, &buffCompiled)
	}

	w.Header().Set("Expires",	 	"Thu, 31 Dec 2016 00:00:00 GMT")
	w.Header().Set("Accept-Ranges",	 "bytes")
	w.Header().Set("Content-type", 	 m.mediaType)
	w.Header().Set("Content-Length", strconv.Itoa(buffCompiled.Len()))
	w.Write(buffCompiled.Bytes())
}

func readFile(file string, buff *bytes.Buffer) {
	absFilePath, err := filepath.Abs(filepath.Dir(file))
	checkErr(err)

	fullFilePath := filepath.Join(absFilePath, filepath.Base(file))
	b, err := ioutil.ReadFile(fullFilePath)
	checkErr(err)

	b = bytes.Replace(b, []byte("  "), []byte(" "), -1)
//	b = bytes.Replace(b, []byte("\n"), []byte(""), -1)
	buff.Write(b)
}

func checkErr(err error) {
	if err != nil {
		log.Fatalf("Erro %s", err)
	}
}